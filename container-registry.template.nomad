job "container-registry" {
  type = "service"
  region = "global"
  datacenters = ["proxima"]

  constraint {
    attribute = "${attr.cpu.arch}"
    operator  = "!="
    value = "arm"
  }

  group "docker-registry" {
    count = 1

    constraint {
      distinct_hosts = true
    }
    network {
      mode = "bridge"
    }

    service {
      name = "docker-registry"
      port = "5000"

      connect {
        sidecar_service {}
      }
    }

    task "docker" {
      driver = "docker"

      config {
        image = "registry:latest"

        volumes = [
          "local/registry:/mnt/registry"
        ]
      }

      template {
        data =<<EOH
REGISTRY_HTTP_SECRET=test123456
        EOH

        destination = "secret/registry.env"
        env = true
      }
    }
  }

  group "docker-dashboard" {
    count = 1

    update {
      max_parallel = 1
      health_check = "checks"
      min_healthy_time = "10s"
      healthy_deadline = "5m"
      progress_deadline = "10m"
      auto_revert       = true
      auto_promote      = true
      stagger = "30s"
      canary = 1
    }

    network {
      mode = "bridge"
    }
    
    service {
      name = "docker-dashboard"
      port = "80"

      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "docker-registry"
              local_bind_port  = 5000
            }
          }
        }
      }
    }

    task "dashboard" {
      driver = "docker"

      config {
        image = "joxit/docker-registry-ui:latest"
      }

      template {
        data =<<EOH
URL=https://registry.docker.hq.carboncollins.se
REGISTRY_URL=https://registry.docker.hq.carboncollins.se
REGISTRY_TITLE=hQTech
DELETE_IMAGES=false
        EOH

        destination = "secrets/ui.env"
        env = true
      }
    }
  }

  group "ingress-gateway" {
    network {
      mode = "bridge"

      port "inbound" { to = 8090 }
      port "inbound-dashboard" { to = 8091 }
    }

    service {
      name = "docker-registry-ingress-service"
      port = "inbound"

      connect {
        gateway {
          proxy {}

          ingress {
            listener {
              port = 8090
              protocol = "tcp"

              service {
                name = "docker-registry"
              }
            }

            tls {
              enabled = true
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.protocol=https",
        "traefik.http.routers.docker-registry.entrypoints=https",
        "traefik.http.routers.docker-registry.rule=Host(`registry.docker.hq.carboncollins.se`)",
        "traefik.http.routers.docker-registry.tls=true",
        "traefik.http.routers.docker-registry.tls.certresolver=lets-encrypt",
        "traefik.http.routers.docker-registry.tls.domains[0].main=registry.docker.hq.carboncollins.se",
        "traefik.http.middlewares.docker-registry.headers.accesscontrolalloworiginlist=https://docker.hq.carboncollins.se",
        "traefik.http.services.docker-registry.loadBalancer.serversTransport=ingress@file",
        "traefik.http.services.docker-registry.loadbalancer.server.scheme=https",
        "hq.service.exposed=true",
        "hq.service.subdomain=registry.docker"
      ]
    }

    service {
      name = "docker-dashboard-ingress-service"
      port = "inbound"

      connect {
        gateway {
          proxy {}

          ingress {
            listener {
              port = 8091
              protocol = "tcp"

              service {
                name = "docker-dashboard"
              }
            }

            tls {
              enabled = true
            }
          }
        }
      }

      tags = [
        "traefik.enable=true",
        "traefik.protocol=https",
        "traefik.http.routers.docker-dashboard.entrypoints=https",
        "traefik.http.routers.docker-dashboard.rule=Host(`docker.hq.carboncollins.se`)",
        "traefik.http.routers.docker-dashboard.tls=true",
        "traefik.http.routers.docker-dashboard.tls.certresolver=lets-encrypt",
        "traefik.http.routers.docker-dashboard.tls.domains[0].main=*.hq.carboncollins.se",
        "traefik.http.services.docker-dashboard.loadBalancer.serversTransport=ingress@file",
        "traefik.http.services.docker-dashboard.loadbalancer.server.scheme=https",
        "hq.service.exposed=true",
        "hq.service.subdomain=docker"
      ]
    }
  }
}